function sayHello(name){
  console.log(`Hello ${name}`);
}

function sayGoodbye(name){
  console.log(`Goodbye ${name}`);
}

module.exports.sayHello = sayHello
exports.sayGoodbye = sayGoodbye
//其他js模块可以通过以下方式调用该js的导出函数
// var commonJs = require('../../utils/common.js')
// commonJs.sayHello('hahahahaha');